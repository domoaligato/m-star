= M-Star Mini installation

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

Installing a M-Star Mini is simple enough that you can do it even if you
are not experienced at tinkering with computers.  Helpfully,
most of the internals of a Mini-M are so rugged that you
would have to do actual work to damage them.

Nevertheless, the plague of lawyers is such that we must include
the following disclaimer:

WARNING: The M-Star Mini developers offer NO WARRANTY on the correct
operation of the M-Star Mini with your keyboard hardware, or against any
damage caused by M-Star Mini installation.  You modify your keyboard AT
YOUR OWN RISK of rendering it inoperable.

It is always good to have a wingman when doing an installation like
this. Your wingman can read the instructions to you as you work and
discuss them with you until you're sure you understand what to do.

With only one exception all these steps are nondestructive and
reversible. Save the parts you remove; in the (very unlikely) event
that the installation fails, it should be possible to reverse the
steps and reinstall your factory controller.

Test your keyboard before you begin dissassembling.  If you come to
think you have a failed upgrade and you didn't explicitly test
beforehand, you'll really wish you had done so.

== Terminology and anatomy

There are some terms you need to know when reading these instructions.
Without them, we'd have to repeat a lot of confusingly similar
descriptive phrases, making them more difficult to follow.

Terms for some things you can see from the outside:

cover::
   The top half of the Model M's case. The part the keys protrude through.

pan::
   The bottom half of the Model M's case.

lock lights::
   Your Model M probably has three LEDs labeled NumLock, CapsLock. and
   ScrollLock; most do, and this means it was made to be used with a
   personal computer.

Here are some things you won't be able to see until you open the case:

include::innerparts.inc[]

Molex plug::
   Molex is a brand name for a family of internal connectors used in a
   lot of electronics. A common alternate term for these is "JST
   plug". There are many different varieties of Molex connectors, but
   in these instructions we mean a particularly common one with 4
   leads enclosed in a rectangular housing of white, semitranslucent
   plastic. There are male and female versions.  Image
   <<molex-and-mickey,here>>.

== Things you will need

* Either a #1 Pozidriv screwdriver or a 5.5m (7/32") thin-walled hex
  driver, depenng on the mini-M's manufacturing revision.

* Small container to keep screws and bolts in so they don't get lost.

* A pair of needle-nosed pliers.

* A pair of tin snips or pair of electrician's sidecutters.  A
  screwdriver with a very small flat blade may also be useful.

* A USB 2.0 full-sized B cable of the kind commonly used with printers
  and scanners (for all variants other than round-top M122s).

* An insulating, anti-static pad. A rubber mousepad or wrist-rest is
  ideal for this. A piece of cardboard or paper is passable.

Choose the other end of your cable to match the ports on whatever
machine you want to connect to - usually this will be A, perhaps
sometimes C. The rest of these instructions are indifferent about
this.

The M-Star Mini has a full-sized female B jack. Your cable needs to
have a full-sized male B plug to match it, not mini- or micro-B. Note
that you do *not* want the SS-B version of a B plug with extra leads
in the connector!  Usually these B cables have a USB A plug on the
other end (as in the images below), but get whatever suits your
computer's ports.

[grid="cols", align="center", width="50%"]
|====================================================================
| image:USB2.jpg[]             | image:USB3.jpg[]
| Yes! This [green]#WILL WORK# | No! This is SS-B and [red]#WILL NOT WORK#
|====================================================================

== First step: Smoke-test the M-Star Mini controller

include::smoketest.inc[]

== Second step: Open the case

Your keyboard may arrive with the cable engaged. If so, it's locked
in; a casual tug will not remove it.  Use a small flat-bladed
scrwdriver inserted into the opening immediately above the jack to
spring the lock.

Unplug your M, turn it upside-down, and find the case screws on the
bottom of the pan near the rear edge.

// The Classic installation guide has an image of a face-down M at
// this point in the exposition.  It's omitted because for the Mini
// it would be a cruel tease - the scew wells are black on black
// and pretty much invisible.

Some Mini-Ms ship with #1 Pozidriv case screws, others with
5.5mm hex-head screws.  You should be able to tell which yours
has by looking in the wells.

Once you've loosened the screws you may find you need to turn the
keyboard right-side up again so they will fall out of the wells.
Do this well away from the edge of your worktable, slowly, so as
not to lose any screws.

When you have put the extracted screws in a safe place, turn the M
right-side up and lift the cover away. You should be looking at the
keys and (behind them) the plate assembly.  You should be able to see
the top edge of the metal backplate protruding from behind the
plastic barrel plate in the front.

== Third step:  Unplug the ribbon cables

Now that you have verified that your controller can be
replaced by the M-Star Mini, turn the case around until you're looking at
the rear of the keyboard.

We recommend that you take a photograph of the rear view with cables
in place before continuing, so you'll be able to revert correctly if
the installation fails.

You will see two 16-pin ribbon cables running from just over the top
of the backplate down to sockets in a PCB sitting underneath it in the
pan.

//.Mini-M M from the rear, showing the ribbon cables.
image::mini-rearview.jpg[]

You will need to detach the ribbon cables connecting the
plate assembly to the PCB. The next move will make that easier.

There should be two posts sticking up from the pan near the interior
left rear and right rear corners, snapped into holes in the metal
backplate.  You can see these in the previous "Cover off" image.

Disengage the backplate from the posts by pulling it carefully upwards.
Let the backplate settle back onto the posts so it sits on top of
them without re-engaging the posts. You may be able to do the next step
- actually pulling the ribbon cables out of their connectors - with
mo more fuss.  If you don't have quite enough room to get purchase on
them, lift the rear edge of the backplate an inch or so with one
hand to get the clearance you need for the other.

Only friction keeps the ribbons in the ribbon sockets; a slow, firm pull
with your fingers should get them out.  Notice that the ribbon
connectors are right-angled and the ribbon cables have to be pulled
out horizontally; if you pull straight up vertically you can cause damage.

While it might seem tempting to use the needlenose pliers to pull
out the ribbon cables, those metal jaws can easily damage them and we
strongly recommend against it.

You have now disconnected the rest of the keyboard from its
electronics and can proceed to the next step.

== Fourth step:  Lift away the plate assembly and remove the PCB

Lift the plate assembly off the pan.

Mini-Ms have one-way friction washers as hold-downs for the PCB.
These are thin circlets of spring steel with inward-pointing prongs
shaped so that they can be pushed down around the mounting peg, but if
you try to pull them back up the prongs dig in.

.One-way friction washers in a Mini-M
image::friction-washers.jpg[]

Trying to force a friction washer upwards and off will just destroy
the mounting peg, because the steel is stronger than the plastic.
It's better to destroy the friction washer instead.  This is the only
irreversible step in the upgrade.

Fortunately, those hold-downs aren't really needed unless your
keyboard is subjected to sufficiently regular and violent mechanical
shocks that you should probably reconsider your lifestyle choices.

You can take out the friction washers with a pair of tin snips, aka
metalcutting dikes. An electrician's sidecutter will probably do,
though it will take more effort and put wear on the sidecutters.

Attack the thinnest sections of the circlet and cut at it until it
loses its lock on the peg. It may be helpful to pry one edge
of the washer upwards with a small flat-bladed screwdriver so
you can get a cutter jaw underneath it.

Alternatively, you may be able to use that flat-bladed
screwdriver or the tip of a knifeblade to bend the prongs of the
washers upwards so that they no longer grip the pegs.  Be gentle, as
breaking those pegs can make the keyboard unusable.

== Fifth step:  Attach the ribbon cables to the M-Star Mini.

include:insertion1.adoc[]

.Ideal starting position
image::ideal-starting-position.jpg[align="center"]

You have an ideal starting position when the ribbon cable ends
are resting directly on the connectors they're to plug into.
You want them to go in as vertically as possible - bending is
not good for the ribbon cables.

If you are right-handed, work left to right so your hand doesn't have
to collide with a ribbon cable you've already plugged in.  If you're
left-handed, go in the other direction.

include:insertion2.adoc[]

.What ribbon cables look like when properly seated
image::ribbonfit.jpg[align="center"]

== Sixth step:  Testing

include::testing.inc[]

End this step by unplugging your USB cable from the M-Star Mini.

== Seventh step: Use mounting tape

If you have mounting tape, there is room between the angled surface on
the shroud and the backplate to stick a small piece of the tape
there as a functiuonal replacement for thr friction-washer hold-downs..

Peel off the backing on the bottom side but on the top; the
idea isn't to stick the tape to both shroud and backplate, just pad
it securely in place when the backplate is in its installed position.

== Eighth step: Seat the M-Star-Mini and put the plate assembly back in place

include::reseat.inc[]

== Ninth step: Final reassembly

Assuming all went well, push the rear pair of holes in the backplate
down over the posts so they snap into place. You might need to wiggle
the plate assembly a bit if it isn't sitting square.

Put the cover back on, invert the keyboard and put the case screws
back in.  You're done.

// end
